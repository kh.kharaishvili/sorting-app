package com.example;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

@RunWith(Parameterized.class)
public class SortingAppTest {

    private final int[] input;
    private final int[] expected;

    public SortingAppTest(int[] input, int[] expected) {
        this.input = input;
        this.expected = expected;
    }

    @Test
    public void testSort() {
        int[] actual = Arrays.copyOf(input, input.length);
        Arrays.sort(actual);
        assertArrayEquals(expected, actual);
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new int[]{}, new int[]{}},                  // Zero arguments
                {new int[]{1}, new int[]{1}},                // One argument
                {new int[]{3, 1, 2}, new int[]{1, 2, 3}},    // Multiple arguments
                {new int[]{9, 5, 7, 2, 4, 1}, new int[]{1, 2, 4, 5, 7, 9}},  // Multiple arguments (unsorted)
                {new int[]{1, 1, 1, 1, 1}, new int[]{1, 1, 1, 1, 1}},        // Duplicate arguments
                {new int[]{10, 9, 8, 7, 6, 5, 4, 3, 2, 1}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},  // Maximum arguments
                {new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},    // Maximum arguments (already sorted)
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testThrowException_whenTooManyArguments() {
        int[] intArgs = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        String[] args = Arrays.stream(intArgs).mapToObj(String::valueOf).toArray(String[]::new);
        SortingApp.main(args);
    }
}
