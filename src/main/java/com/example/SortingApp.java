package com.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class SortingApp {

    private static final Logger logger = LogManager.getLogger(SortingApp.class);

    /**
     * @param args
     */
    public static void main(String[] args) {
        SortingApp sortingApp = new SortingApp();
        int[] sortedNumbers = sortingApp.sortNumbers(args);
        logger.info("Sorted numbers: " + Arrays.toString(sortedNumbers));
    }

    /**
     * @param args
     * @return
     */
    public int[] sortNumbers(String[] args) {
        if (args.length == 0) {
            logger.error("No arguments provided.");
        }
        if (args.length > 10) {
            throw new IllegalArgumentException("The number of elements is more then 10");
        }

        int[] numbers = new int[args.length];
        try {
            for (int i = 0; i < args.length; i++) {
                numbers[i] = Integer.parseInt(args[i]);
            }

        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid argument. Not a number");
        }
        Arrays.sort(numbers);
        return numbers;

    }
}
